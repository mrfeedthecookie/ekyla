package fr.ekyla.core.spell;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;

import java.util.List;

public abstract class SpellNoTarget extends Spell {

    public SpellNoTarget(String name, ChatColor color, EkylaPlayer player, int manaCost, int level, List<String> description) {
        super(name, color, player, manaCost, level, description);
    }

    public abstract void use();

}
