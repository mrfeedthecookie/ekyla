package fr.ekyla.core.spell.spells;

import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.spell.SpellNoTarget;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.util.Vector;

import java.util.Arrays;

public class FireballSpell extends SpellNoTarget {

    public FireballSpell(EkylaPlayer player, int level) {
        super("Boule de feu", ChatColor.GOLD, player, 10, level, Arrays.asList("§6Faites appel à la puissance des volcans", "§6pour lancez une boule enflamée."));
    }

    @Override
    public void use() {
        Location start = getPlayer().getPlayer().getEyeLocation();
        Vector increase = start.getDirection();

        for (int counter = 0; counter <= 100; counter++) {
            Location point = start.add(increase);
            getPlayer().getPlayer().getWorld().spawnParticle(Particle.FLAME, point, 1, 0F, 0F, 0F, 0.001);
            if (point.getBlock().getType() != Material.AIR) {
                point.getWorld().createExplosion(point.getX(), point.getY(), point.getZ(), (getLevel() >= 10 ? 10 : getLevel()), false, false);
                break;
            }
        }
    }
}
