package fr.ekyla.core.inventories.listener;

import fr.ekyla.core.Core;
import fr.ekyla.core.inventories.AbstractInventory;
import fr.ekyla.core.npc.messages.BanquierMessages;
import fr.ekyla.core.npc.messages.BucheronMessages;
import fr.ekyla.core.npc.messages.CharpentierMessages;
import fr.ekyla.core.npc.messages.MaquignonMessages;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.ItemStack;

public class InventoryListener implements Listener {

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {
        ItemStack itemStack = event.getCurrentItem();
        if (itemStack == null || itemStack.getType().equals(Material.AIR)) return;
        if (!itemStack.hasItemMeta()) return;
        AbstractInventory inventory = AbstractInventory.getInventory(event.getInventory().getTitle());
        if (inventory == null) return;
        event.setCancelled(true);
        inventory.onClick(EkylaPlayer.getPlayer((Player) event.getWhoClicked()), itemStack);
    }

    @EventHandler
    public void onInventoryOpen(InventoryOpenEvent event) {
        EkylaPlayer player = EkylaPlayer.getPlayer((Player) event.getPlayer());
        if (event.getInventory().getTitle().startsWith(player.getMontures().getName())) {
            event.setCancelled(true);
            player.getPlayer().openInventory(Bukkit.createInventory(null, 0, "Chevauche " + player.getMontures().getName()));
        }
        if (event.getInventory().getTitle().startsWith("§0Bucheron")) {
            if (player.getPlayer().getWorld().getTime() >= 23000 || player.getPlayer().getWorld().getTime() <= 13000) {
                player.sendMessage("§0Bucheron §7»", BucheronMessages.getBucheronRandomMessage(BucheronMessages.openinventoryday).replaceAll("%pname", player.getRPName() + ""));
            } else {
                player.sendMessage("§0Bucheron §7»", BucheronMessages.getBucheronRandomMessage(BucheronMessages.openinventorynight).replaceAll("%pname", player.getRPName() + ""));
            }
        }

        if (event.getInventory().getTitle().startsWith("§6Banquier")) {
            if (player.getPlayer().getWorld().getTime() >= 23000 || player.getPlayer().getWorld().getTime() <= 13000) {
                player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.openinventoryday).replaceAll("%pname", player.getRPName() + ""));
            } else {
                player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.openinventorynight).replaceAll("%pname", player.getRPName() + ""));
            }
        }

        if (event.getInventory().getTitle().startsWith("§0Charpentier")) {
            if (player.getPlayer().getWorld().getTime() >= 23000 || player.getPlayer().getWorld().getTime() <= 13000) {
                player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.openinventoryday).replaceAll("%pname", player.getRPName() + ""));
            } else {
                player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.openinventorynight).replaceAll("%pname", player.getRPName() + ""));
            }
        }

        if (event.getInventory().getTitle().startsWith("§8Maquignon")) {
            if (player.getPlayer().getWorld().getTime() >= 23000 || player.getPlayer().getWorld().getTime() <= 13000) {
                player.sendMessage("§8Maquignon §7»", MaquignonMessages.getMaquignonRandomMessage(MaquignonMessages.openinventoryday).replaceAll("%pname", player.getRPName() + ""));
            } else {
                player.sendMessage("§8Maquignon §7»", MaquignonMessages.getMaquignonRandomMessage(MaquignonMessages.openinventorynight).replaceAll("%pname", player.getRPName() + ""));
            }
        }
    }

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {
        EkylaPlayer player = EkylaPlayer.getPlayer((Player) event.getPlayer());
        if (event.getInventory().getTitle().startsWith("§0Bucheron")) {
            if (player.getPlayer().getWorld().hasStorm()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§0Bucheron §7»", BucheronMessages.getBucheronRandomMessage(BucheronMessages.closeinventoryrainy).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            } else {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§0Bucheron §7»", BucheronMessages.getBucheronRandomMessage(BucheronMessages.closeinventorysunny).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            }
        }

        if (event.getInventory().getTitle().startsWith("§6Banquier")) {
            if (player.getMoney() != 0) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.closeinventorymoney).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            } else {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§6Banquier §7»", BanquierMessages.getBanquierRandomMessage(BanquierMessages.closeinventorynomoney).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            }
        }

        if (event.getInventory().getTitle().startsWith("§0Charpentier")) {
            if (player.getPlayer().getWorld().hasStorm()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.closeinventoryrainy).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            } else {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§0Charpentier §7»", CharpentierMessages.getCharpentierRandomMessage(CharpentierMessages.closeinventorysunny).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            }
        }

        if (event.getInventory().getTitle().startsWith("§8Maquignon")) {
            if (player.getPlayer().getWorld().hasStorm()) {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§8Maquignon §7»", MaquignonMessages.getMaquignonRandomMessage(MaquignonMessages.closeinventoryrainy).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            } else {
                Bukkit.getScheduler().scheduleSyncDelayedTask(Core.getInstance(), () -> {
                    player.sendMessage("§8Maquignon §7»", MaquignonMessages.getMaquignonRandomMessage(MaquignonMessages.closeinventorysunny).replaceAll("%pname", player.getRPName() + ""));
                }, (long) 10);
            }
        }
    }
}