package fr.ekyla.core.npc.trade;

import fr.ekyla.core.items.Itembuilder;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;

public class CharpentierTrades {

    public static void setCharpentierAchatItems(EkylaPlayer player) {
        Inventory inventory = player.getPlayer().getOpenInventory().getTopInventory();
        inventory.setItem(53, new Itembuilder(Material.BOOK).setName("§6Passer en mode Vente").setLore(Arrays.asList("§6Contenu de votre bourse: §e" + player.getMoney() + " PO")).buildItemStack());
        inventory.setItem((0 * 9) + 0, new Itembuilder(Material.WOOD).setData(0).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 1, new Itembuilder(Material.WOOD).setData(1).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 2, new Itembuilder(Material.WOOD).setData(2).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 3, new Itembuilder(Material.WOOD).setData(3).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 4, new Itembuilder(Material.WOOD).setData(4).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 5, new Itembuilder(Material.WOOD).setData(5).setLore(Arrays.asList("§6Acheter: §e15 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOD_DOOR).setLore(Arrays.asList("§6Acheter: §e30 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 1, new Itembuilder(Material.SPRUCE_DOOR_ITEM).setLore(Arrays.asList("§6Acheter: §e30 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 2, new Itembuilder(Material.BIRCH_DOOR_ITEM).setLore(Arrays.asList("§6Acheter: §e30 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 3, new Itembuilder(Material.JUNGLE_DOOR_ITEM).setLore(Arrays.asList("§6Acheter: §e30 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 4, new Itembuilder(Material.ACACIA_DOOR_ITEM).setLore(Arrays.asList("§6Acheter: §e30 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 5, new Itembuilder(Material.DARK_OAK_DOOR_ITEM).setLore(Arrays.asList("§6Acheter: §e30 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 0, new Itembuilder(Material.FENCE).setLore(Arrays.asList("§6Acheter: §e25 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 1, new Itembuilder(Material.SPRUCE_FENCE).setLore(Arrays.asList("§6Acheter: §e25 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 2, new Itembuilder(Material.BIRCH_FENCE).setLore(Arrays.asList("§6Acheter: §e25 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 3, new Itembuilder(Material.JUNGLE_FENCE).setLore(Arrays.asList("§6Acheter: §e25 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 4, new Itembuilder(Material.ACACIA_FENCE).setLore(Arrays.asList("§6Acheter: §e25 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 5, new Itembuilder(Material.DARK_OAK_FENCE).setLore(Arrays.asList("§6Acheter: §e25 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 0, new Itembuilder(Material.FENCE_GATE).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 1, new Itembuilder(Material.SPRUCE_FENCE_GATE).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 2, new Itembuilder(Material.BIRCH_FENCE_GATE).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 3, new Itembuilder(Material.JUNGLE_FENCE_GATE).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 4, new Itembuilder(Material.ACACIA_FENCE_GATE).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 5, new Itembuilder(Material.DARK_OAK_FENCE_GATE).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 0, new Itembuilder(Material.STICK).setLore(Arrays.asList("§6Acheter: §e7.5 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 1, new Itembuilder(Material.TRAP_DOOR).setLore(Arrays.asList("§6Acheter: §e45 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 2, new Itembuilder(Material.LADDER).setLore(Arrays.asList("§6Acheter: §e17.5 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 3, new Itembuilder(Material.CHEST).setLore(Arrays.asList("§6Acheter: §e120 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 4, new Itembuilder(Material.WORKBENCH).setLore(Arrays.asList("§6Acheter: §e60 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 5, new Itembuilder(Material.SIGN).setData(0).setLore(Arrays.asList("§6Acheter: §e32.5 PO")).buildItemStack());
    }

    public static void setCharpentierVenteItems(EkylaPlayer player) {
        Inventory inventory = player.getPlayer().getOpenInventory().getTopInventory();
        inventory.setItem(53, new Itembuilder(Material.BOOK).setName("§6Passer en mode Achat").setLore(Arrays.asList("§6Contenu de votre bourse: §e" + player.getMoney() + " PO")).buildItemStack());
        inventory.setItem((0 * 9) + 0, new Itembuilder(Material.WOOD).setData(0).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 1, new Itembuilder(Material.WOOD).setData(1).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 2, new Itembuilder(Material.WOOD).setData(2).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 3, new Itembuilder(Material.WOOD).setData(3).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 4, new Itembuilder(Material.WOOD).setData(4).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((0 * 9) + 5, new Itembuilder(Material.WOOD).setData(5).setLore(Arrays.asList("§6Vendre: §e12 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 0, new Itembuilder(Material.WOOD_DOOR).setLore(Arrays.asList("§6Vendre: §e24 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 1, new Itembuilder(Material.SPRUCE_DOOR_ITEM).setLore(Arrays.asList("§6Vendre: §e24 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 2, new Itembuilder(Material.BIRCH_DOOR_ITEM).setLore(Arrays.asList("§6Vendre: §e24 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 3, new Itembuilder(Material.JUNGLE_DOOR_ITEM).setLore(Arrays.asList("§6Vendre: §e24 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 4, new Itembuilder(Material.ACACIA_DOOR_ITEM).setLore(Arrays.asList("§6Vendre: §e24 PO")).buildItemStack());
        inventory.setItem((1 * 9) + 5, new Itembuilder(Material.DARK_OAK_DOOR_ITEM).setLore(Arrays.asList("§6Vendre: §e24 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 0, new Itembuilder(Material.FENCE).setLore(Arrays.asList("§6Vendre: §e20 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 1, new Itembuilder(Material.SPRUCE_FENCE).setLore(Arrays.asList("§6Vendre: §e20 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 2, new Itembuilder(Material.BIRCH_FENCE).setLore(Arrays.asList("§6Vendre: §e20 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 3, new Itembuilder(Material.JUNGLE_FENCE).setLore(Arrays.asList("§6Vendre: §e20 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 4, new Itembuilder(Material.ACACIA_FENCE).setLore(Arrays.asList("§6Vendre: §e20 PO")).buildItemStack());
        inventory.setItem((2 * 9) + 5, new Itembuilder(Material.DARK_OAK_FENCE).setLore(Arrays.asList("§6Vendre: §e20 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 0, new Itembuilder(Material.FENCE_GATE).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 1, new Itembuilder(Material.SPRUCE_FENCE_GATE).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 2, new Itembuilder(Material.BIRCH_FENCE_GATE).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 3, new Itembuilder(Material.JUNGLE_FENCE_GATE).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 4, new Itembuilder(Material.ACACIA_FENCE_GATE).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((3 * 9) + 5, new Itembuilder(Material.DARK_OAK_FENCE_GATE).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 0, new Itembuilder(Material.STICK).setLore(Arrays.asList("§6Vendre: §e6 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 1, new Itembuilder(Material.TRAP_DOOR).setLore(Arrays.asList("§6Vendre: §e36 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 2, new Itembuilder(Material.LADDER).setLore(Arrays.asList("§6Vendre: §e14 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 3, new Itembuilder(Material.CHEST).setLore(Arrays.asList("§6Vendre: §e96 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 4, new Itembuilder(Material.WORKBENCH).setLore(Arrays.asList("§6Vendre: §e48 PO")).buildItemStack());
        inventory.setItem((4 * 9) + 5, new Itembuilder(Material.SIGN).setData(0).setLore(Arrays.asList("§6Vendre: §e26 PO")).buildItemStack());
    }
}
