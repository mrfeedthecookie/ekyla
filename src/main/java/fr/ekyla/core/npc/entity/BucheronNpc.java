package fr.ekyla.core.npc.entity;

import fr.ekyla.core.npc.MerchantNpc;
import org.bukkit.Location;
import org.bukkit.entity.Villager;

import java.util.Random;

public class BucheronNpc extends MerchantNpc {

    public BucheronNpc(String name, Location location) {
        super("bucheron", name, location, new Random().nextInt(150));
    }

    @Override
    public void spawn() {
        npc = location.getWorld().spawn(location, Villager.class);
        Villager villager = (Villager) npc;
        villager.setProfession(Villager.Profession.NITWIT);
        villager.getLocation().setYaw(location.getYaw());
        villager.getLocation().setPitch(location.getPitch());
        villager.setAdult();
        villager.setAI(false);
        villager.setCollidable(false);
        villager.setAgeLock(true);
        villager.setCustomName(getName());
        villager.setCustomNameVisible(true);
        villager.setInvulnerable(true);
        villager.teleport(location);
    }
}
