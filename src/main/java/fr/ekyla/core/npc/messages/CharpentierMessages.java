package fr.ekyla.core.npc.messages;

import java.util.Random;

public class CharpentierMessages {

    public static final String[] buyfail = new String[]{
            "Hola %pname ! Il vous manque de l'argent pour acheter ceci !",
            "Comment ? vous pensez pouvoir acheter cela avec seulement §e%money PO§7."
    };
    public static final String[] buysucess = new String[]{
            "Et voila, le tout pour seulement §e%price PO§7.",
            "Et voici, comme demandé !"
    };
    public static final String[] sellfail = new String[]{
            "Hola %pname ! Tentez-vous de me voler ? Vous n'avez pas ce que je demande.",
            "Si vous ne me présentez pas la marchandise attendue, je ne peux vous payer."
    };
    public static final String[] sellsucess = new String[]{
            "Et voici vos §e%price PO §7comme convenu.",
            "Je vous en donne pour §e%price PO §7, une excellente affaire !"
    };
    public static final String[] openinventoryday = new String[]{
            "Bonjour %pname ! que puis-je faire pour vous ?",
            "%pname ! Quel plaisir me vaut cette visite ?"
    };
    public static final String[] openinventorynight = new String[]{
            "Bonsoir %pname ! que puis-je faire pour vous ?",
            "%pname ! Quel plaisir me vaut cette visite ?"
    };
    public static final String[] closeinventoryrainy = new String[]{
            "A bientôt %pname",
            "Bonne continuation %pname !"
    };
    public static final String[] closeinventorysunny = new String[]{
            "A bientôt %pname.",
            "Au revoir et profitez en tant qu'il fait beau !"
    };

    public static String getCharpentierRandomMessage(String[] array) {
        if (array.length == 0) return "";
        Random random = new Random();
        int nb = random.nextInt(array.length);
        String message = array[nb];
        if (message == null || message.equals("")) {
            System.out.println("Erreur : getCharpentierRandomMessage");
            return "";
        }
        return message;
    }
}
