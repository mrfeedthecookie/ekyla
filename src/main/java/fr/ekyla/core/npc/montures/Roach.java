package fr.ekyla.core.npc.montures;

import fr.ekyla.core.players.EkylaPlayer;
import net.minecraft.server.v1_12_R1.AttributeInstance;
import net.minecraft.server.v1_12_R1.GenericAttributes;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftLivingEntity;
import org.bukkit.entity.Horse;
import org.bukkit.inventory.ItemStack;

public class Roach extends Montures{

    public Roach(EkylaPlayer player, String type, String name, String color, String style, String armor, float speed, float jump, float health) {
        super(player, type, name, color, style, armor, speed, jump, health);
    }

    public static void spawnRoach(EkylaPlayer player) {
        Horse horse = player.getPlayer().getWorld().spawn(player.getLocationUtil().getLocation(), Horse.class);
        horse.setAdult();
        horse.setAgeLock(true);
        horse.setCustomName("Roach");
        horse.setCustomNameVisible(true);
        horse.setInvulnerable(true);
        horse.setColor(Horse.Color.BROWN);
        horse.setStyle(Horse.Style.WHITE);
        horse.setTamed(true);
        horse.getInventory().setSaddle(new ItemStack(Material.SADDLE));
        horse.setJumpStrength(1);
        horse.setMaxHealth(10);
        horse.setHealth(10);
        AttributeInstance attributes = (((CraftLivingEntity) horse).getHandle()).getAttributeInstance(GenericAttributes.MOVEMENT_SPEED);
        attributes.setValue(0.5);
        horse.addPassenger(player.getPlayer());
        player.getMontures().setMonture(horse);
    }
}