package fr.ekyla.core.players;

import fr.ekyla.core.Core;
import fr.ekyla.core.npc.montures.Montures;
import fr.ekyla.core.players.bank.BankAccount;
import fr.ekyla.core.players.factions.FactionRoles;
import fr.ekyla.core.players.skills.Skill;
import fr.ekyla.core.spell.Spell;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PlayerUtils {

    public static void clearPlayerInventory(Player player) {
        player.getInventory().clear();
        player.getInventory().setItemInMainHand(null);
        player.getInventory().setItemInOffHand(null);
        player.getInventory().setHelmet(null);
        player.getInventory().setChestplate(null);
        player.getInventory().setLeggings(null);
        player.getInventory().setBoots(null);
    }

    public static void createAccount(Player player) {
        try {
            final Connection connection = Core.getMainDatabase().getConnection();

            PreparedStatement select = connection.prepareStatement("SELECT id FROM players WHERE uuid='" + player.getUniqueId().toString() + "'");
            select.executeQuery();
            ResultSet set = select.getResultSet();
            if (set.next()) {
                connection.close();
                return;
            }
            PreparedStatement statement = connection.prepareStatement("INSERT INTO `players` (`uuid`, `username`, `account_configured`, `rpname`, `power`, `level`, `money`, `faction_id`, `classe`) VALUES ('" + player.getUniqueId().toString() + "', '" + player.getName() + "', 0, '', 0, 1, 0, 1, 'vagabond');");
            statement.executeUpdate();
            connection.close();
        } catch (SQLException e) {
            player.kickPlayer("§cUne erreur est survenue lors de la création de votre profil.\n§cSi ce problème persiste, transmetez ce code d'erreur à un développeur/administrateur (0005)");
        }
    }

    public static void updatePlayer(EkylaPlayer player) {
        try {
            final Connection connection = Core.getMainDatabase().getConnection();
            int quest = 0;
            if (player.getQuest() != null) quest = player.getQuest().getId();

            StringBuilder builder = new StringBuilder();

            if (player.getSpells().size() != 0) {
                for (Spell spell : player.getSpells()) {
                    builder.append(spell.getClass().getSimpleName().replace("Spell", "")).append(":").append(spell.getLevel()).append(",");
                }
            }

            PreparedStatement preparedStatement = connection.prepareStatement("UPDATE players SET account_configured='" + player.getAccountConfigured() + "', power='" + player.getRank().getPower() + "', level='" + player.getLevel() + "', money='" + player.getMoney() + "', faction_id='" + player.getFactionId() + "', faction_role='" + player.getFactionRole().toString().toLowerCase() + "', mana='" + player.getMana() + "', classe='" + player.getClasse().getName() + "', questId='" + quest + "', questStepId='" + player.getQuestStep() + "', spells='" + builder.toString() + "' WHERE uuid='" + player.getUuid().toString() + "'");
            preparedStatement.executeUpdate();

            PreparedStatement rpnameprepareStatement = connection.prepareStatement("UPDATE players SET rpname='" + player.getRPName() + "' WHERE uuid='" + player.getUuid().toString() + "'");
            rpnameprepareStatement.executeUpdate();

            PreparedStatement skills = connection.prepareStatement("UPDATE skills SET crochetage=" + player.getSkills().getCrochetage() + ", spell=" + player.getSkills().getSpell() + ", brute=" + player.getSkills().getBrute() + ", vol=" + player.getSkills().getVol() + " WHERE uuid='" + player.getUuid().toString() + "'");
            skills.executeUpdate();

            if (player.getMontures() != null) {
                PreparedStatement montureStatement = connection.prepareStatement("UPDATE monture SET type='" + player.getMontures().getType() + "', name='" + player.getMontures().getName() + "', color='" + player.getMontures().getColor() + "', style='" + player.getMontures().getStyle() + "', armor='" + player.getMontures().getArmor() + "', speed='" + player.getMontures().getSpeed() + "', jump='" + player.getMontures().getJump() + "', health='" + player.getMontures().getHealth() + "' WHERE uuid='" + player.getUuid().toString() + "'");
                montureStatement.executeUpdate();
            }
            if (player.getMontureData() != null) {
                PreparedStatement montureDataStatement = connection.prepareStatement("UPDATE monture_data SET ColorWHITE='" + player.getMontureData().getColorWHITE() + "', ColorCREAMY='" + player.getMontureData().getColorCREAMY() + "', ColorCHESTNUT='" + player.getMontureData().getColorCHESTNUT() + "', ColorBROWN='" + player.getMontureData().getColorBROWN() + "', ColorBLACK='" + player.getMontureData().getColorBLACK() + "', ColorGRAY='" + player.getMontureData().getColorGRAY() + "', ColorDARK_BROWN='" + player.getMontureData().getColorDARK_BROWN() + "', StyleNONE='" + player.getMontureData().getStyleNONE() + "', StyleWHITE='" + player.getMontureData().getStyleWHITE() + "', StyleWHITEFIELD='" + player.getMontureData().getStyleWHITEFIELD() + "', StyleWHITE_DOTS='" + player.getMontureData().getStyleWHITE_DOTS() + "', StyleBLACK_DOTS='" + player.getMontureData().getStyleBLACK_DOTS() + "', ArmorIRON_BARDING='" + player.getMontureData().getArmorIRON_BARDING() + "', ArmorGOLD_BARDING='" + player.getMontureData().getArmorGOLD_BARDING() + "', ArmorDIAMOND_BARDING='" + player.getMontureData().getArmorDIAMOND_BARDING() + "' WHERE uuid='" + player.getUuid().toString() + "'");
                montureDataStatement.executeUpdate();
            }
            if (player.hasAccount()) {
                PreparedStatement bankAccount = connection.prepareStatement("UPDATE bank_account SET faction_id=" + player.getFactionId() + ", money=" + player.getBankAccount().getMoney() + ", imposition=" + player.getBankAccount().getImposition() + " WHERE uuid='" + player.getUuid().toString() + "'");
                bankAccount.executeUpdate();
            }
            connection.close();
        } catch (SQLException e) {
            System.out.println("§cUne erreur est survenue lors de l'enregistrement du profil de " + player.getName() + " (0004)");
        }
    }

    public static EkylaPlayer getAccount(Player player) {
        if (EkylaPlayer.getPlayer(player) != null) return EkylaPlayer.getPlayer(player);
        EkylaPlayer ekylaPlayer = null;
        try {
            final Connection connection = Core.getMainDatabase().getConnection();
            PreparedStatement statement = connection.prepareStatement("SELECT * FROM players WHERE uuid='" + player.getUniqueId().toString() + "'");
            statement.executeQuery();
            ResultSet resultSet = statement.getResultSet();
            if (resultSet.next()) {
                int mana = resultSet.getInt("mana");
                RankList rankList = RankList.getFromPower(resultSet.getInt("power"));
                int accountConfigured = resultSet.getInt("account_configured");
                String RPName = resultSet.getString("rpname");
                int factionId = resultSet.getInt("faction_id");
                FactionRoles factionRole = FactionRoles.valueOf(resultSet.getString("faction_role").toUpperCase());
                if (factionRole == null) factionRole = FactionRoles.DEFAULT;
                String classe = resultSet.getString("classe");
                int level = resultSet.getInt("level");
                double money = resultSet.getDouble("money");
                int quest = resultSet.getInt("questId");
                int questStep = resultSet.getInt("questStepId");

                ekylaPlayer = new EkylaPlayer(player, rankList, accountConfigured, RPName, factionId, factionRole, classe, mana, level, money, quest, questStep);

                //Spells Data
                String[] spells = resultSet.getString("spells").split(",");
                for (String spell1 : spells) {
                    if (spell1.equalsIgnoreCase("")) break;
                    String className = spell1.split(":")[0];
                    int spellLevel = Integer.parseInt(spell1.split(":")[1]);
                    try {
                        Class<?> clazz = Class.forName("fr.ekyla.core.spell.spells." + className + "Spell");
                        Constructor<?> constructor = clazz.getConstructor(EkylaPlayer.class, int.class);
                        Spell spell = (Spell) constructor.newInstance(ekylaPlayer, spellLevel);
                        ekylaPlayer.addSpell(spell);
                    } catch (ClassNotFoundException | NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException ignored) {
                        player.kickPlayer("§cUne erreur en survenue lors de la récupération de votre profil. (0006)");
                    }
                }

                //Skills
                PreparedStatement skills = connection.prepareStatement("SELECT * FROM skills WHERE uuid='" + player.getUniqueId().toString() + "'");
                skills.executeQuery();
                ResultSet skillsSet = skills.getResultSet();
                if (skillsSet.next()) {
                    ekylaPlayer.setSkills(new Skill(ekylaPlayer, skillsSet.getDouble("crochetage"), skillsSet.getDouble("spell"), skillsSet.getDouble("brute"), skillsSet.getDouble("vol")));
                } else {
                    PreparedStatement skills2 = connection.prepareStatement("INSERT INTO skills (uuid, crochetage, spell, brute, vol) VALUES ('" + player.getUniqueId().toString() + "', 1, 1, 1, 1)");
                    skills2.executeUpdate();
                    ekylaPlayer.setSkills(new Skill(ekylaPlayer, 1.0, 1.0, 1.0, 1.0));
                }

                //Bank
                PreparedStatement bank = connection.prepareStatement("SELECT * FROM bank_account WHERE uuid='" + player.getUniqueId().toString() + "'");
                bank.executeQuery();
                ResultSet bankSet = bank.getResultSet();
                if (bankSet.next()) {
                    ekylaPlayer.setBankAccount(new BankAccount(ekylaPlayer, bankSet.getFloat("imposition"), bankSet.getDouble("money"), ekylaPlayer.getFaction()));
                }

                //Monture
                PreparedStatement montures = connection.prepareStatement("SELECT * FROM monture WHERE uuid='" + player.getUniqueId().toString() + "'");
                montures.executeQuery();
                ResultSet monturesSet = montures.getResultSet();
                if (monturesSet.next()) {
                    ekylaPlayer.setMontures(new Montures(ekylaPlayer, monturesSet.getString("type"), monturesSet.getString("name"), monturesSet.getString("color"), monturesSet.getString("style"), monturesSet.getString("armor"), monturesSet.getFloat("speed"), monturesSet.getFloat("jump"), monturesSet.getFloat("health")));
                } else {
                    PreparedStatement montures2 = connection.prepareStatement("INSERT INTO monture (uuid, type, name, color, style, armor, speed, jump, health) VALUES ('" + player.getUniqueId().toString() + "', 'Horses', 'Monture', 'BLACK', 'NONE', 'IRON_BARDING', 0.25, 0.5, 10)");
                    montures2.executeUpdate();
                    ekylaPlayer.setMontures(new Montures(ekylaPlayer, "Horses", "Monture", "BLACK", "NONE", "IRON_BARDING", 0.25F, 0.5F, 10F));
                }

                //MontureData
                PreparedStatement montureData = connection.prepareStatement("SELECT * FROM monture_data WHERE uuid='" + player.getUniqueId().toString() + "'");
                montureData.executeQuery();
                ResultSet montureDataSet = montureData.getResultSet();
                if (montureDataSet.next()) {
                    ekylaPlayer.setMontureData(new MontureData(ekylaPlayer, montureDataSet.getInt("ColorWHITE"), montureDataSet.getInt("ColorCREAMY"), montureDataSet.getInt("ColorCHESTNUT"), montureDataSet.getInt("ColorBROWN"), montureDataSet.getInt("ColorBLACK"), montureDataSet.getInt("ColorGRAY"), montureDataSet.getInt("ColorDARK_BROWN"), montureDataSet.getInt("StyleNONE"), montureDataSet.getInt("StyleWHITE"), montureDataSet.getInt("StyleWHITEFIELD"), montureDataSet.getInt("StyleWHITE_DOTS"), montureDataSet.getInt("StyleBLACK_DOTS"), montureDataSet.getInt("ArmorIRON_BARDING"), montureDataSet.getInt("ArmorGOLD_BARDING"), montureDataSet.getInt("ArmorDIAMOND_BARDING")));
                } else {
                    PreparedStatement montureData2 = connection.prepareStatement("INSERT INTO monture_data (uuid, ColorWHITE, ColorCREAMY, ColorCHESTNUT, ColorBROWN, ColorBLACK, ColorGRAY, ColorDARK_BROWN, StyleNONE, StyleWHITE, StyleWHITEFIELD, StyleWHITE_DOTS, StyleBLACK_DOTS, ArmorIRON_BARDING, ArmorGOLD_BARDING, ArmorDIAMOND_BARDING) VALUES ('" + player.getUniqueId().toString() + "', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0)");
                    montureData2.executeUpdate();
                    ekylaPlayer.setMontureData(new MontureData(ekylaPlayer, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0));
                }
            } else {
                createAccount(player);
                ekylaPlayer = getAccount(player);
            }
            connection.close();
        } catch (SQLException e) {
            player.kickPlayer("§cUne erreur en survenue lors de la récupération de votre profil. (0007)");
        }
        return ekylaPlayer;
    }

}
