package fr.ekyla.core.players.bank;

import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.players.factions.Faction;

public class BankAccount {

    private EkylaPlayer player;
    private float imposition;
    private double money;
    private Faction faction;

    public BankAccount(EkylaPlayer player, float imposition, double money, Faction faction) {
        this.player = player;
        this.imposition = imposition;
        this.money = money;
        this.faction = faction;
    }

    /**
     * get the player
     *
     * @return player
     */
    public EkylaPlayer getPlayer() {
        return player;
    }

    /**
     * get the imposition
     *
     * @return imposition
     */
    public float getImposition() {
        return imposition;
    }

    /**
     * set the imposition
     *
     * @param imposition the new imposition
     */
    public void setImposition(float imposition) {
        this.imposition = imposition;
    }

    public float getPlayerImpisition() {
        return imposition + player.getFaction().getImposition();
    }

    /**
     * get the money
     *
     * @return money
     */
    public double getMoney() {
        return money;
    }

    /**
     * set the money
     *
     * @param money money
     */
    public void setMoney(double money) {
        this.money = money;
    }

    /**
     * Add money to the current player
     *
     * @param money the money to add
     */
    public void addMoney(double money) {
        this.money += money;
    }

    /**
     * Remove money from the current player
     *
     * @param money the money to remove
     */
    public void removeMoney(double money) {
        this.money -= money;
    }

    /**
     * has the player a positive money if remove the amount
     *
     * @param amount amount
     * @return
     */
    public boolean hasMoney(double amount) {
        return (getMoney() - amount) >= 0;
    }
}
