package fr.ekyla.core.players;

public class MontureData {

    private EkylaPlayer player;
    private int ColorWHITE;
    private int ColorCREAMY;
    private int ColorCHESTNUT;
    private int ColorBROWN;
    private int ColorBLACK;
    private int ColorGRAY;
    private int ColorDARK_BROWN;
    private int StyleNONE;
    private int StyleWHITE;
    private int StyleWHITEFIELD;
    private int StyleWHITE_DOTS;
    private int StyleBLACK_DOTS;
    private int ArmorIRON_BARDING;
    private int ArmorGOLD_BARDING;
    private int ArmorDIAMOND_BARDING;

    public MontureData(EkylaPlayer player, int ColorWHITE, int ColorCREAMY, int ColorCHESTNUT, int ColorBROWN, int ColorBLACK, int ColorGRAY, int ColorDARK_BROWN, int StyleNONE, int StyleWHITE, int StyleWHITEFIELD, int StyleWHITE_DOTS, int StyleBLACK_DOTS, int ArmorIRON_BARDING, int ArmorGOLD_BARDING, int ArmorDIAMOND_BARDING) {
        this.player = player;
        this.ColorWHITE = ColorWHITE;
        this.ColorCREAMY = ColorCREAMY;
        this.ColorCHESTNUT = ColorCHESTNUT;
        this.ColorBROWN = ColorBROWN;
        this.ColorBLACK = ColorBLACK;
        this.ColorGRAY = ColorGRAY;
        this.ColorDARK_BROWN = ColorDARK_BROWN;
        this.StyleNONE = StyleNONE;
        this.StyleWHITE = StyleWHITE;
        this.StyleWHITEFIELD = StyleWHITEFIELD;
        this.StyleWHITE_DOTS = StyleWHITE_DOTS;
        this.StyleBLACK_DOTS = StyleBLACK_DOTS;
        this.ArmorIRON_BARDING = ArmorIRON_BARDING;
        this.ArmorGOLD_BARDING = ArmorGOLD_BARDING;
        this.ArmorDIAMOND_BARDING = ArmorDIAMOND_BARDING;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public void setPlayer(EkylaPlayer player) {
        this.player = player;
    }

    public int getColorWHITE() {
        return ColorWHITE;
    }

    public void setColorWHITE(int colorWHITE) {
        ColorWHITE = colorWHITE;
    }

    public int getColorCREAMY() {
        return ColorCREAMY;
    }

    public void setColorCREAMY(int colorCREAMY) {
        ColorCREAMY = colorCREAMY;
    }

    public int getColorCHESTNUT() {
        return ColorCHESTNUT;
    }

    public void setColorCHESTNUT(int colorCHESTNUT) {
        ColorCHESTNUT = colorCHESTNUT;
    }

    public int getColorBROWN() {
        return ColorBROWN;
    }

    public void setColorBROWN(int colorBROWN) {
        ColorBROWN = colorBROWN;
    }

    public int getColorBLACK() {
        return ColorBLACK;
    }

    public void setColorBLACK(int colorBLACK) {
        ColorBLACK = colorBLACK;
    }

    public int getColorGRAY() {
        return ColorGRAY;
    }

    public void setColorGRAY(int colorGRAY) {
        ColorGRAY = colorGRAY;
    }

    public int getColorDARK_BROWN() {
        return ColorDARK_BROWN;
    }

    public void setColorDARK_BROWN(int colorDARK_BROWN) {
        ColorDARK_BROWN = colorDARK_BROWN;
    }

    public int getStyleNONE() {
        return StyleNONE;
    }

    public void setStyleNONE(int styleNONE) {
        StyleNONE = styleNONE;
    }

    public int getStyleWHITE() {
        return StyleWHITE;
    }

    public void setStyleWHITE(int styleWHITE) {
        StyleWHITE = styleWHITE;
    }

    public int getStyleWHITEFIELD() {
        return StyleWHITEFIELD;
    }

    public void setStyleWHITEFIELD(int styleWHITEFIELD) {
        StyleWHITEFIELD = styleWHITEFIELD;
    }

    public int getStyleWHITE_DOTS() {
        return StyleWHITE_DOTS;
    }

    public void setStyleWHITE_DOTS(int styleWHITE_DOTS) {
        StyleWHITE_DOTS = styleWHITE_DOTS;
    }

    public int getStyleBLACK_DOTS() {
        return StyleBLACK_DOTS;
    }

    public void setStyleBLACK_DOTS(int styleBLACK_DOTS) {
        StyleBLACK_DOTS = styleBLACK_DOTS;
    }

    public int getArmorIRON_BARDING() {
        return ArmorIRON_BARDING;
    }

    public void setArmorIRON_BARDING(int armorIRON_BARDING) {
        ArmorIRON_BARDING = armorIRON_BARDING;
    }

    public int getArmorGOLD_BARDING() {
        return ArmorGOLD_BARDING;
    }

    public void setArmorGOLD_BARDING(int armorGOLD_BARDING) {
        ArmorGOLD_BARDING = armorGOLD_BARDING;
    }

    public int getArmorDIAMOND_BARDING() {
        return ArmorDIAMOND_BARDING;
    }

    public void setArmorDIAMOND_BARDING(int armorDIAMOND_BARDING) {
        ArmorDIAMOND_BARDING = armorDIAMOND_BARDING;
    }
}
