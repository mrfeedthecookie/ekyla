package fr.ekyla.core.players.factions;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public class Faction {

    private static List<Faction> factions = new ArrayList<>();
    private int id;
    private String name;
    private ChatColor color;
    private String displayName;
    private List<EkylaPlayer> members;
    private float imposition;
    private int money;
    private double level;

    public Faction(int id, String name, String colorCode, float imposition, int money, double level) {
        this.id = id;
        this.name = name;
        this.color = ChatColor.getByChar(colorCode);
        this.displayName = color + name;
        this.members = new ArrayList<>();
        this.imposition = imposition;
        this.money = money;
        this.level = level;

        factions.add(this);
    }

    /**
     * get all factions
     *
     * @return factions
     */
    public static List<Faction> getFactions() {
        return factions;
    }

    /**
     * return the imposition of the faction
     *
     * @return imposition
     */
    public float getImposition() {
        return imposition;
    }

    /**
     * set the imposition of the faction
     *
     * @param imposition the new imposition
     */
    public void setImposition(float imposition) {
        this.imposition = imposition;
    }

    /**
     * return the money of the faction
     *
     * @return money
     */
    public int getMoney() {
        return money;
    }

    /**
     * set the money of the faction
     *
     * @param money the new money
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     * get a faction by it name
     *
     * @param name the name
     * @return the faction
     */
    public Faction getFaction(String name) {
        for (Faction factions : factions) if (factions.getName().equalsIgnoreCase(name)) return factions;
        return null;
    }

    /**
     * return the id of the faction
     *
     * @return id
     */
    public int getId() {
        return id;
    }

    /**
     * return the name of the faction
     *
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * return the color of the faction
     *
     * @return color
     */
    public ChatColor getColor() {
        return color;
    }

    /**
     * return the formatted name of the faction
     * color + name
     *
     * @return displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * put all members of this faction in the memebers list
     */
    private void initializeMembers() {
        members.clear();
        for (EkylaPlayer players : EkylaPlayer.getEkylaPlayers()) {
            if (players.getFactionId() == id) members.add(players);
        }
    }

    public void addMember(EkylaPlayer player) {
        getFactions().forEach(faction -> {
            if (faction.getMembers().contains(player))
                faction.removeMember(player);
        });
        getMembers().add(player);
        player.setFactionId(id);
    }

    public void removeMember(EkylaPlayer player) {
        getMembers().remove(player);
        player.setFactionId(1);
        Faction.getFactions().get(0).addMember(player);
    }

    public List<EkylaPlayer> getMembers() {
        return members;
    }

    @Override
    public String toString() {
        return "(Id: " + id + ", name: " + name + ", color: " + color + ", displayName: " + displayName + ")";
    }

    public double getLevel() {
        return level;
    }

    public void setLevel(double level) {
        this.level = level;
    }

    public void addLevel(double level) {
        this.level += level;
    }
}
