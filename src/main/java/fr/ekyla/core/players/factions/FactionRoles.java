package fr.ekyla.core.players.factions;

public enum FactionRoles {

    CHIEF(100, "Chef"),

    DEFAULT(0, "Nouveau"),;

    private int power;
    private String name;

    FactionRoles(int power, String name) {
        this.power = power;
        this.name = name;
    }

    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }
}
