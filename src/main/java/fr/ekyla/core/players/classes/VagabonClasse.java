package fr.ekyla.core.players.classes;

import fr.ekyla.core.players.EkylaPlayer;

public class VagabonClasse extends Classe {

    public VagabonClasse(EkylaPlayer player) {
        super(player, 0, "vagabond");
    }

    @Override
    public void pouvoir() {
        getPlayer().sendMessage("§6Petite voix §7» §7§oVous n'avez pas de pouvoir");
    }
}
