package fr.ekyla.core.listener;

import org.bukkit.entity.Creeper;
import org.bukkit.entity.EnderCrystal;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityExplodeEvent;

public class WorldListener implements Listener {

    @EventHandler
    public void onExplode(EntityExplodeEvent event) {
        System.out.println("entity explode");
        if (event.getEntity() instanceof Creeper || event.getEntity() instanceof EnderCrystal) {
            event.setCancelled(true);
            event.getEntity().remove();
        }
    }

    @EventHandler
    public void onHit(EntityDamageEvent event) {
        if (event.getEntityType().equals(EntityType.ENDER_CRYSTAL)) {
            //event.setCancelled(true);
            //event.getEntity().remove();
        }
    }

}
