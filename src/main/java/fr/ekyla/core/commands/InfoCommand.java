package fr.ekyla.core.commands;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {
        if (commandSender instanceof Player) {
            EkylaPlayer player = EkylaPlayer.getPlayer((Player) commandSender);
            if (args.length == 0) {
                player.sendMessage(player.toString());
            } else if (args.length == 1) {
                if (!player.hasPermission("players.info")) {
                    player.sendMessage("&cErreur, faites /info");
                    return false;
                }
                EkylaPlayer target = EkylaPlayer.getPlayer(args[0]);
                if (target == null) player.sendMessage("&cErreur, ce joueur n'est pas connecté");
                else player.sendMessage(target.toString());
            } else {
                player.sendMessage("&cUtilisation: /info");
            }
        } else {
            if (args.length != 1) {
                commandSender.sendMessage("§cUtilisation: /info joueur");
                return false;
            } else {
                EkylaPlayer target = EkylaPlayer.getPlayer(args[0]);
                if (target == null) commandSender.sendMessage(ChatColor.RED + "Erreur, ce joueur n'est pas connecté");
                else commandSender.sendMessage(ChatColor.translateAlternateColorCodes('§', target.toString()));
            }
        }
        return false;
    }
}
