package fr.ekyla.core.commands;

import fr.ekyla.core.npc.AbstractNpc;
import fr.ekyla.core.npc.saver.NPCSave;
import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class NpcCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            EkylaPlayer player = EkylaPlayer.getPlayer((Player) sender);
            if (player.hasPermission("npc.spawn")) {
                if (args.length == 0) {
                    player.sendMessage("§cUtilisation: /npc <type> <nom> | /npc save <id>");
                    return false;
                } else if (args[0].equalsIgnoreCase("save")) {
                    int id = Integer.valueOf(args[1]);
                    AbstractNpc.getNpcList().get(id).save(NPCSave.GOLD);
                } else if (args[0].equalsIgnoreCase("new")) {
                    String npcType = args[1];
                    StringBuilder name = new StringBuilder();
                    for (int i = 2; i < args.length; i++) {
                        name.append(" ").append(args[i]);
                    }
                    npcType = npcType.substring(0, 1).toUpperCase() + npcType.substring(1).toLowerCase();
                    try {
                        Class<?> npcClass = Class.forName("fr.ekyla.core.npc.entity." + npcType + "Npc");
                        Constructor<?> npc = npcClass.getConstructor(String.class, Location.class);
                        AbstractNpc abstractNpc = (AbstractNpc) npc.newInstance(ChatColor.translateAlternateColorCodes('&', name.toString()), player.getPlayer().getLocation());
                        abstractNpc.spawn();
                    } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException | ClassNotFoundException e) {
                        player.sendMessage("§cUne erreur est survenue, verifier le type de npc puis réessayez (" + npcType + ").");
                    }
                } else {
                    player.sendMessage("§cUtilisez: /npc new <npcType> | /npc save <id>");
                }
            } else player.sendMessage("§cErreur, vous n'avez pas la permission d'executer cette commande");
        } else {
            sender.sendMessage(ChatColor.RED + "Erreur, vous devez être un joueur");
        }
        return false;
    }
}
