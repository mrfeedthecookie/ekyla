package fr.ekyla.core.commands;

import fr.ekyla.core.players.EkylaPlayer;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class RPRenameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            EkylaPlayer player = EkylaPlayer.getPlayer((Player) sender);
            if (player.hasPermission("rprename.player")) {
                if (args.length == 0) {
                    player.sendMessage("§cUtilisation: /rprename <joueur> <RPName>");
                } else {
                    EkylaPlayer target = EkylaPlayer.getPlayer(args[0]);
                    if (target == null) player.sendMessage("&cErreur, ce joueur n'est pas connecté");
                    else {
                        StringBuilder name = new StringBuilder();
                        for (int i = 1; i < args.length; i++) {
                            name.append(" ").append(args[i]);
                        }
                        String RPName = name.toString().substring(1);
                        if (RPName.length() > 32) {
                            player.sendMessage("&cErreur, ce nom est trop long");
                        } else {
                            target.setRPName(RPName);
                        }
                    }

                }
            } else player.sendMessage("§cErreur, vous n'avez pas la permission d'executer cette commande");
        } else {
            sender.sendMessage(ChatColor.RED + "Erreur, vous devez être un joueur");
        }
        return false;
    }
}
