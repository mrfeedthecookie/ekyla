package fr.ekyla.core.quests.steps;

import fr.ekyla.core.quests.AQuest;

public abstract class AQuestStep {

    private int id;
    private String name;
    private AQuest mainQuest;

    public AQuestStep(int id, String name, AQuest mainQuest) {
        this.id = id;
        this.name = name;
        this.mainQuest = mainQuest;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public AQuest getMainQuest() {
        return mainQuest;
    }

    protected abstract void onStart();

    protected abstract void onFinish();

    public final void start() {
        getMainQuest().getPlayer().sendMessage("§6Quête §7»", "Vous commencez l'étape: §6" + getName() + "§7.");
        onStart();
    }

    public final void finish() {
        onFinish();
        mainQuest.nextStep();
    }

}
