package fr.ekyla.core.quests;

import fr.ekyla.core.players.EkylaPlayer;
import fr.ekyla.core.players.chat.Message;
import fr.ekyla.core.players.chat.RPChat;
import fr.ekyla.core.quests.steps.AQuestStep;

import java.util.ArrayList;
import java.util.List;

public abstract class AQuest {

    private List<AQuestStep> steps = new ArrayList<>();
    private int id;
    private String name;
    private int currentStep;
    private EkylaPlayer player;

    public AQuest(int id, String name, int currentStep, EkylaPlayer player) {
        this.id = id;
        this.name = name;
        this.currentStep = currentStep;
        this.player = player;
        this.steps.addAll(registerSteps());
        start();
    }

    public AQuest getObject() {
        return this;
    }

    protected abstract List<AQuestStep> registerSteps();

    protected abstract void onStart();

    protected abstract void onFinish();

    public String getName() {
        return name;
    }

    public int getCurrentStepId() {
        return currentStep;
    }

    public EkylaPlayer getPlayer() {
        return player;
    }

    public int getId() {
        return id;
    }

    public List<AQuestStep> getSteps() {
        return steps;
    }

    public AQuestStep getStep(int id) {
        for (AQuestStep step : steps) if (step.getId() == id) return step;
        return steps.get(0);
    }

    public AQuestStep getCurrentStep() {
        return getStep(currentStep);
    }

    public void nextStep() {
        if (getSteps().size() > getCurrentStepId()) {
            getPlayer().sendMessage("§6Quête §7»", "Vous avez finis l'étape §6" + getCurrentStep().getName() + "§7.");
            currentStep++;
            player.setQuestStep(currentStep);
            getCurrentStep().start();
        } else finish();
    }

    public final void start() {
        player.sendMessage("§6Quête §7»", "Vous commencez la quête §6" + getName() + "§7.");
        onStart();
        player.setQuestStep(currentStep);
        getCurrentStep().start();
    }

    public final void finish() {
        player.sendMessage("§6Quête §7»", "Vous avez terminé la quête §6" + getName() + "§7.");
        player.setQuest(null);
        player.setQuestStep(0);
        onFinish();
    }

    public void startChat(List<Message> messages, Callback<EkylaPlayer> callback) {
        player.getInfo().setRpChat(new RPChat(player, messages, callback).nextMessage());
    }

    public void nextMessage() {
        if (player.getInfo().getRpChat() == null) return;
        player.getInfo().getRpChat().nextMessage();
    }

}
